package keeptrax;

import java.util.ArrayList;
import java.util.List;

import keeptrax.utility.DateUtility;
import keeptrax.utility.Utility;
import keeptrax.vo.UserPlacesVO;
import keeptrax.vo.UserVO;
import keeptrax.vo.UserVisitsVO;

import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class DBoperation {
	
	DateUtility dateUtility = new DateUtility();
	Utility utility = new Utility();
	
	public UserVO getUserDetails(DB dbConn,String emailID){
		
		UserVO userVO = new UserVO();
		
		DBCollection table = dbConn.getCollection("users");
		BasicDBObject search = new BasicDBObject();
		search.put("email", emailID);
		
		DBCursor cursor = table.find(search);
		
		
		
		int rowIndex = 0;
		while(cursor.hasNext()){
			ArrayList<String> list = new ArrayList<>();
			JSONObject json;
			try {
				json = (JSONObject)new JSONParser().parse(cursor.next().toString());
				
				String name = (String)json.get("email");
				String userObjectID = utility.convertObjectIDToString(json.get("_id").toString());

				userVO.setEmail(name);
				userVO.setUserObjectID(userObjectID);
				
				} catch (ParseException e) {
				e.printStackTrace();
			}
			
			//get places details from userplaces collection START				
				List<UserPlacesVO> userPlacesList =	getUserPlaces(dbConn,userVO.getUserObjectID());		
				userVO.setUserPlacesVOList(userPlacesList);
			//END
			
			}
		
		return userVO;
	}
	
	
	public List<UserPlacesVO> getUserPlaces(DB dbConn ,String userObjectID){
		
		ArrayList<UserPlacesVO> userPlaceVOList = new ArrayList<>();		
		DBCollection tableUserPlaces = dbConn.getCollection("userplaces");		
		BasicDBObject searchUserPlace = new BasicDBObject();
		searchUserPlace.put("account", ObjectId.massageToObjectId(userObjectID));
		DBCursor cursorUserPlace = tableUserPlaces.find(searchUserPlace);		
		
		while(cursorUserPlace.hasNext()){
			
			 JSONObject jsonUserPlace;
			 UserPlacesVO userPlacesVO = new UserPlacesVO();
			 
			try {
				jsonUserPlace = (JSONObject)new JSONParser().parse(cursorUserPlace.next().toString());
				
				//JSONObject json = (JSONObject)new JSONParser().parse(jsonUserPlace.get("_id").toString());				
				//userPlacesVO.setPlaceID((json.get("$oid").toString()));			
				
				userPlacesVO.setUserObjectID(utility.convertObjectIDToString(jsonUserPlace.get("account").toString()));
				userPlacesVO.setPlaceID(utility.convertObjectIDToString(jsonUserPlace.get("_id").toString()));
				//userPlacesVO.setUserEmailID((String)jsonUserPlace.get("user"));
				userPlacesVO.setCountry((String)jsonUserPlace.get("country"));
				userPlacesVO.setCity((String)jsonUserPlace.get("locality"));
				userPlacesVO.setStreet((String)jsonUserPlace.get("sublocality"));
				 
				 
				//get userVisits.
				List<UserVisitsVO> userVisitsVOList =getUserVisits(dbConn,
											userPlacesVO.getUserObjectID(),
											userPlacesVO.getPlaceID());
				
				userPlacesVO.setUserVisitsVOList(userVisitsVOList);
				
				//end userVisits.
				
				userPlaceVOList.add(userPlacesVO);
				 
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return userPlaceVOList;
	}
	
	public List<UserVisitsVO> getUserVisits(DB dbConn,String userID,String placeID) throws ParseException{
		
		UserVisitsVO userVisitsVO = null;
		ArrayList<UserVisitsVO> userVisitsVOList = new ArrayList<>();
		
		DBCollection tableUserVisits = dbConn.getCollection("visits");		
		BasicDBObject searchUserVisits = new BasicDBObject();
		
		searchUserVisits.put("account", ObjectId.massageToObjectId(userID));
		searchUserVisits.put("userPlace", ObjectId.massageToObjectId(placeID));
		
		DBCursor cursorUserVisits = tableUserVisits.find(searchUserVisits);		
		
		while(cursorUserVisits.hasNext()){
			
			 JSONObject jsonUserVisits;			 
			 
			try {
				jsonUserVisits = (JSONObject)new JSONParser().parse(cursorUserVisits.next().toString());
				userVisitsVO = new UserVisitsVO();
				
				
				userVisitsVO.setUserEmailID(
								utility.convertObjectIDToString(
										jsonUserVisits.get("account").toString()));
				
				
				String[] strStartDateTime = dateUtility.deserializeEntryDate(jsonUserVisits.get("entryTime"));
				userVisitsVO.setVisitDate(strStartDateTime[0]);
				userVisitsVO.setEntryTime(strStartDateTime[1]);
				
				
				String[] strExitDateTime = dateUtility.deserializeEntryDate(jsonUserVisits.get("exitTime"));				
				userVisitsVO.setExitTime(strExitDateTime[1]);
				userVisitsVOList.add(userVisitsVO);
				
    			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return userVisitsVOList;
	}
	
}
