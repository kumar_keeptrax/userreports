package keeptrax;

import java.io.*; 
import java.text.*;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

public class HTMLTable{
JTable table;
public static void main(String[] args) {
	HTMLTable table  = new HTMLTable();
	table.CreateTable();
}

public void CreateTable(){
JFrame frame = new JFrame("Create table in java");
JPanel panel = new JPanel();
String data[][] = {{"Vinod","Computer"},
{"Ravi","Computer"},
{"Deepak","Biology"},{"Santosh","Photoshop"},{"Amardeep","MCA"},{"Suman","MCA"}};
String col [] = {"Name","Course"};
DefaultTableModel modeltable = new DefaultTableModel(data,col);
table = new JTable(modeltable);
Dimension dimen = new Dimension(20,1);
table.setIntercellSpacing(new Dimension(dimen));
SetRowHight(table);
table.setColumnSelectionAllowed(true);
JTableHeader head = table.getTableHeader();
head.setBackground(Color.pink);
JScrollPane pane = new JScrollPane(table);
panel.add(pane);
frame.add(panel);
frame.setSize(500,230);
frame.setUndecorated(true);
frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);
}
public void SetRowHight(JTable table){
int height = table.getRowHeight();
table.setRowHeight(height+10);
}
}