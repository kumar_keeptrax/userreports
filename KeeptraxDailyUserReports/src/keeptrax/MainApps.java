package keeptrax;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoURI;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import keeptrax.utility.DateUtility;
import keeptrax.utility.Utility;
import keeptrax.vo.UserPlacesVO;
import keeptrax.vo.UserVO;
import keeptrax.vo.UserVisitsVO;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class MainApps implements Job{
//public class MainApps {
	   
	//public static void main(String[] args) {
public void execute(JobExecutionContext context) throws JobExecutionException {
		 try{   
			 System.out.println("Main Job Started - " + new Date());
				DBoperation dbOperation = new DBoperation();
				Utility utility = new Utility();
				
				String[] userEmails = utility.getPropValues();	
				
				String[] dbProperties = utility.getDBProperties();
							
				DBConnection dbConnection = new DBConnection(dbProperties);			
				DB db  = dbConnection.getDBConnection();
				int rowIndex = 0;
				for(int i=0;i<userEmails.length;i++){
					//get User details.
					UserVO userVO = dbOperation.getUserDetails(db,userEmails[i]);					
					
					rowIndex = rowIndex+1;
					ExcellTable excellTable = new ExcellTable();
					excellTable.generateDocument(userVO,rowIndex,utility);			
			}
			
	      }catch(Exception e){
		     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		  }

	}

}
