package keeptrax.scheduler;


import keeptrax.MainApps;
import keeptrax.email.SendAttachment;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.*;
class SchedulerProcess {

    public static void main(String[] args) {

        try {
        	
            // Grab the Scheduler instance from the Factory 
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            System.out.println("Scheduler Process STARTED");
            
         // define the job and tie it to our HelloJob class
            JobDetail job = newJob(MainApps.class)
                .withIdentity("job1", "group1")
                .build();

            // Trigger the job to run now, and then repeat every 1 min      

            Trigger trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(20))            
                    .build();
            
            
            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(job, trigger);
            
           // System.out.println("Scheduler Process Report Generation ENDED");

          /* try {
				Thread.sleep(90L * 1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
            
            //*************email scheduler START
         /*
            JobDetail jobEmail = newJob(SendAttachment.class)
                .withIdentity("job2", "group2")
                .build();

            Trigger triggerEmail = newTrigger()
                    .withIdentity("trigger2", "group2")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInMinutes(1))            
                    .build();
            
            scheduler.scheduleJob(jobEmail, triggerEmail);*/
            // ********** email scheduler END
            
            System.out.println("Scheduler Process ENDED");
           // scheduler.shutdown(true);

        } catch (SchedulerException se) {
            se.printStackTrace();
        }
    }
}
