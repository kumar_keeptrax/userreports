package keeptrax;

import  java.io.*;  
import java.math.BigDecimal;
import java.util.List;

import keeptrax.utility.Utility;
import keeptrax.vo.UserPlacesVO;
import keeptrax.vo.UserVO;
import keeptrax.vo.UserVisitsVO;

import  org.apache.poi.hssf.usermodel.HSSFSheet;  
import  org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import  org.apache.poi.hssf.usermodel.HSSFRow;
import  org.apache.poi.hssf.usermodel.HSSFCell;  

public class ExcellTable{
	
	//String filename="c:/reports/"+1+".xls" ;
	HSSFWorkbook hwb=new HSSFWorkbook();
	HSSFSheet sheet =  hwb.createSheet("sheet");
	
	FileOutputStream fileOut = null;
	HSSFRow row= null;
	
	
	
    public void  generateDocument(UserVO userVO, int rowIndex,Utility utility){
		try{
		String filename="c:/reports/"+userVO.getEmail()+".xls" ;
		/* HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet("new sheet");*/
		
		HSSFRow rowhead=   sheet.createRow((short)0);
		rowhead.createCell((short) 0).setCellValue("Name");
		rowhead.createCell((short) 1).setCellValue("Street");
		rowhead.createCell((short) 2).setCellValue("City");
		rowhead.createCell((short) 3).setCellValue("Country");
		rowhead.createCell((short) 4).setCellValue("Date");
		rowhead.createCell((short) 5).setCellValue("Start Time");
		rowhead.createCell((short) 6).setCellValue("End Time");
		rowhead.createCell((short) 7).setCellValue("Duration");
		
		
		//HSSFRow row=   sheet.createRow((short)1);
		
		 
		 //
		 if(userVO !=null){
			List<UserPlacesVO>  userPlacesVOList = userVO.getUserPlacesVOList();
			
			for(int j=0;j< userPlacesVOList.size();j++){
				
				
				
				UserPlacesVO userPlacesVO =  userPlacesVOList.get(j);
				String country = userPlacesVO.getCountry();
				String city = userPlacesVO.getCity();
				String street = userPlacesVO.getStreet();
								 
				List<UserVisitsVO>  userVisitsVOList = userPlacesVO.getUserVisitsVOList();
				 for(int k=0;k<userVisitsVOList.size();k++){
					 
					 row =   sheet.createRow((short) k+1); //row creation
					 
					 UserVisitsVO userVisitsVO = userVisitsVOList.get(k);
					 String visitDate = userVisitsVO.getVisitDate();
					 String entryTime = userVisitsVO.getEntryTime();
					 String exitTime = userVisitsVO.getExitTime();
					 
					 //BigDecimal duration = utility.getDurationDiff(entryTime, exitTime);
					 
					if(utility.isEntryTimeValid(entryTime)){					 
					 //add data to excell				
					 
					 row.createCell((short) 0).setCellValue(userVO.getEmail());
					 row.createCell((short) 1).setCellValue(street);
					 row.createCell((short) 2).setCellValue(city);
					 row.createCell((short) 3).setCellValue(country);
					 row.createCell((short) 4).setCellValue(visitDate);
					 row.createCell((short) 5).setCellValue(entryTime);
					 row.createCell((short) 6).setCellValue(exitTime);
					 row.createCell((short) 7).setCellValue("");
				 
					 
					}else{
						row =   sheet.createRow((short) 1);
						row.createCell((short) 1).setCellValue("No Valid Data Available between 9AM - 5PM");
						fileOut = new FileOutputStream(filename,true);
						hwb.write(fileOut);
						fileOut.close();
					}
				 }
			}
			
			 fileOut = new FileOutputStream(filename,true);
			 hwb.write(fileOut);
			 fileOut.close();
		}
		 
		 //
		 
		 
		/*for(int i=0;i<list.size();i++){
			
			System.out.println("List Inde size : " + list.size());
			row.createCell((short) i).setCellValue(list.get(i));
		}*/
		//fileOut = new FileOutputStream(filename,true);
		//BufferedWriter bufferWritter = new BufferedWriter(new OutputStreamWriter(fileOut));
        //bufferWritter.write(data);
		//hwb.write(fileOut);
		
		//fileOut.close();
		System.out.println("Your excel file has been generated!");
		
		} catch ( Exception ex ) {
		    ex.printStackTrace();
		
		}
	}
}