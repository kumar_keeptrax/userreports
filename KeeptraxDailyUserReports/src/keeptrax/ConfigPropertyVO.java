package keeptrax;

public class ConfigPropertyVO {

	//database config properties
	private String dbDomain = null;  
	private int dbPort = 0;
	private String dbDatabase = null;
	private String dbURI = null;	
	private String dbUsername = null;
	private char[] dbPassword = null;
	
	
	private String validEntryTime;
	private String validExitTime;
	
	
	/**
	 * @return the dbDomain
	 */
	public String getDbDomain() {
		return dbDomain;
	}
	/**
	 * @param dbDomain the dbDomain to set
	 */
	public void setDbDomain(String dbDomain) {
		this.dbDomain = dbDomain;
	}
	/**
	 * @return the dbPort
	 */
	public int getDbPort() {
		return dbPort;
	}
	/**
	 * @param dbPort the dbPort to set
	 */
	public void setDbPort(int dbPort) {
		this.dbPort = dbPort;
	}
	/**
	 * @return the dbDatabase
	 */
	public String getDbDatabase() {
		return dbDatabase;
	}
	/**
	 * @param dbDatabase the dbDatabase to set
	 */
	public void setDbDatabase(String dbDatabase) {
		this.dbDatabase = dbDatabase;
	}
	/**
	 * @return the dbURI
	 */
	public String getDbURI() {
		return dbURI;
	}
	/**
	 * @param dbURI the dbURI to set
	 */
	public void setDbURI(String dbURI) {
		this.dbURI = dbURI;
	}
	/**
	 * @return the dbUsername
	 */
	public String getDbUsername() {
		return dbUsername;
	}
	/**
	 * @param dbUsername the dbUsername to set
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	/**
	 * @return the dbPassword
	 */
	public char[] getDbPassword() {
		return dbPassword;
	}
	/**
	 * @param dbPassword the dbPassword to set
	 */
	public void setDbPassword(char[] dbPassword) {
		this.dbPassword = dbPassword;
	}
	/**
	 * @return the validEntryTime
	 */
	public String getValidEntryTime() {
		return validEntryTime;
	}
	/**
	 * @param validEntryTime the validEntryTime to set
	 */
	public void setValidEntryTime(String validEntryTime) {
		this.validEntryTime = validEntryTime;
	}
	/**
	 * @return the validExitTime
	 */
	public String getValidExitTime() {
		return validExitTime;
	}
	/**
	 * @param validExitTime the validExitTime to set
	 */
	public void setValidExitTime(String validExitTime) {
		this.validExitTime = validExitTime;
	}

	
	
	
    
    
    
}
