package keeptrax;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoURI;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;

import java.net.UnknownHostException;
import java.util.List;

import keeptrax.utility.Utility;

public class DBConnection {
	
	
	

	/**
	 * @param args
	 */

    
	/*static final String domain = "localhost";  
    static final int port = 27017;
    static final String database = "test";
    static final String username = "application";
    static final char[] password = "kumar1234!".toCharArray();
    */
    String[] dbConProperites;
    
   // static final String uri =  "mongodb://application:kumar1234!@172.31.6.40:27017/keeptrax_v1,172.31.11.101:27017/keeptrax_v1,172.31.6.165:27017/keeptrax_v1";
    
   // static final MongoURI uri =  new MongoURI("mongodb://54.201.63.155:27017");
    
    DB db = null;
	public DBConnection(String[] dbProperties) {
		this.dbConProperites = dbProperties;
	}
	public DB getDBConnection() {
		 try{   
			 String domain = dbConProperites[0];
			 int port = Integer.valueOf(dbConProperites[1]);
			 String uri = dbConProperites[2];
			 String database = dbConProperites[3];
			 String username = dbConProperites[4];
			 char[] password = dbConProperites[5].toCharArray();
			 
	        MongoClient mongoClient = new MongoClient( domain , port );			
	        
	        db = mongoClient.getDB( database );
	         
		    System.out.println("Connect to database successfully: " + db.getName());	        
			 
			
				
	      }catch(UnknownHostException e){
			     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			  }catch(NumberFormatException e){
				     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				  }catch(Exception e){
		     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		  }
		 return db;
	}

}
