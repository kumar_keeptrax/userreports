package keeptrax.vo;

public class UserVisitsVO {

	private String userEmailID;
	
	private String placeID;
	
	private String entryTime;
	
	private String exitTime;
	
	private String visitDate;
	
	
	/**
	 * @return the visitDate
	 */
	public String getVisitDate() {
		return visitDate;
	}

	/**
	 * @param visitDate the visitDate to set
	 */
	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}

	/**
	 * @return the userEmailID
	 */
	public String getUserEmailID() {
		return userEmailID;
	}

	/**
	 * @param userEmailID the userEmailID to set
	 */
	public void setUserEmailID(String userEmailID) {
		this.userEmailID = userEmailID;
	}

	/**
	 * @return the placeID
	 */
	public String getPlaceID() {
		return placeID;
	}

	/**
	 * @param placeID the placeID to set
	 */
	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	/**
	 * @return the entryTime
	 */
	public String getEntryTime() {
		return entryTime;
	}

	/**
	 * @param entryTime the entryTime to set
	 */
	public void setEntryTime(String entryTime) {
		this.entryTime = entryTime;
	}

	/**
	 * @return the exitTime
	 */
	public String getExitTime() {
		return exitTime;
	}

	/**
	 * @param exitTime the exitTime to set
	 */
	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}

	
	
}
