package keeptrax.vo;

import java.util.List;

public class UserVO {
	
	private String email;
	
	private String firstName;
	
	private String lastName;
	
	private String userObjectID;
	
	List<UserPlacesVO> userPlacesVOList;
	
	/**
	 * @return the userObjectID
	 */
	public String getUserObjectID() {
		return userObjectID;
	}

	/**
	 * @param userObjectID the userObjectID to set
	 */
	public void setUserObjectID(String userObjectID) {
		this.userObjectID = userObjectID;
	}

	/**
	 * @return the userPlacesVOList
	 */
	public List<UserPlacesVO> getUserPlacesVOList() {
		return userPlacesVOList;
	}

	/**
	 * @param userPlacesVOList the userPlacesVOList to set
	 */
	public void setUserPlacesVOList(List<UserPlacesVO> userPlacesVOList) {
		this.userPlacesVOList = userPlacesVOList;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	
	
	
}
