package keeptrax.vo;

import java.util.List;

public class UserPlacesVO {

	private String placeID;
	private String userEmailID;
	private String country;
	private String city;
	private String street;
	private String userObjectID;
	
	/**
	 * @return the userObjectID
	 */
	public String getUserObjectID() {
		return userObjectID;
	}
	/**
	 * @param userObjectID the userObjectID to set
	 */
	public void setUserObjectID(String userObjectID) {
		this.userObjectID = userObjectID;
	}
	List<UserVisitsVO> userVisitsVOList;
	
	UserVisitsVO userVisitsVO;
	/**
	 * @return the userVisitsVO
	 */
	public UserVisitsVO getUserVisitsVO() {
		return userVisitsVO;
	}
	/**
	 * @param userVisitsVO the userVisitsVO to set
	 */
	public void setUserVisitsVO(UserVisitsVO userVisitsVO) {
		this.userVisitsVO = userVisitsVO;
	}
	/**
	 * @return the userVisitsVOList
	 */
	public List<UserVisitsVO> getUserVisitsVOList() {
		return userVisitsVOList;
	}
	/**
	 * @param userVisitsVOList the userVisitsVOList to set
	 */
	public void setUserVisitsVOList(List<UserVisitsVO> userVisitsVOList) {
		this.userVisitsVOList = userVisitsVOList;
	}
	/**
	 * @return the placeID
	 */
	public String getPlaceID() {
		return placeID;
	}
	/**
	 * @param placeID the placeID to set
	 */
	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}
	/**
	 * @return the userEmailID
	 */
	public String getUserEmailID() {
		return userEmailID;
	}
	/**
	 * @param userEmailID the userEmailID to set
	 */
	public void setUserEmailID(String userEmailID) {
		this.userEmailID = userEmailID;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
}
