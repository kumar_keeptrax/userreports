package keeptrax.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtility {

	public void getEmailAttachements(){
		  
		Utility utilityFile = new Utility();
		StringBuffer strToAddress = new StringBuffer();
		//String to= null;
		
		
		
	  //Get the session object  
	  Properties props = new Properties();  
	  props.put("mail.smtp.host", "smtp.gmail.com");  
	  props.put("mail.smtp.socketFactory.port", "465");  
	  props.put("mail.smtp.socketFactory.class",  
	            "javax.net.ssl.SSLSocketFactory");  
	  props.put("mail.smtp.auth", "true");  
	  props.put("mail.smtp.port", "465"); 
	  
	 final String user = utilityFile.getFromEmailPropValues();
	 final String password = utilityFile.getPassword();
	 String[] toArray = utilityFile.getToEmailPropValues();
	 
	 String fileDirectory = utilityFile.getUserReportDir();
	  
	  Session session = Session.getDefaultInstance(props,  
	   new javax.mail.Authenticator() {  
	   protected PasswordAuthentication getPasswordAuthentication() {  
	   return new PasswordAuthentication(user,password);  
	   }  
	  });  
	     
	  //2) compose message     
	  try{  
	    MimeMessage message = new MimeMessage(session);  
	    message.setFrom(new InternetAddress(user));  
	    
	    for(int i=0;i<toArray.length;i++){
	    	
	   	 message.addRecipient(Message.RecipientType.TO,new InternetAddress(toArray[i]));  
	    	
	    }
	    
	    message.setSubject("Keeptrax - Daily User Visit Reports");  
	      
	    //3) create MimeBodyPart object and set your message text     
	    BodyPart messageBodyPart1 = new MimeBodyPart();  
	    messageBodyPart1.setText("Please find the attached user visit reports from Keeptrax App.\n" + "Reports Generated on : " + new Date());  
	    	
	    File firlDir = new File(fileDirectory);
	    
	    List<String> fileList = getFileList(firlDir);//return the list of file list from directory
	    
	  //5) create Multipart object and add MimeBodyPart objects to this object
	    Multipart multipart = new MimeMultipart();  
	    multipart.addBodyPart(messageBodyPart1);  
	      
	    
	    if(fileList.size() > 0){
	    	for(int i=0;i<fileList.size();i++){
	    		//4) create new MimeBodyPart object and set DataHandler object to this object      
	    	    MimeBodyPart messageBodyPart2 = new MimeBodyPart(); 
	    	    
	    	    	String fileName = fileDirectory + "/" + fileList.get(i).toString();
	    	    
	    		 	DataSource source = new FileDataSource(fileName);  
	    		    messageBodyPart2.setDataHandler(new DataHandler(source));  
	    		    messageBodyPart2.setFileName(fileName); 
	    		    
	    		    multipart.addBodyPart(messageBodyPart2);
	    	}
	    }
	    
	     	          
	    //6) set the multiplart object to the message object  
	    message.setContent(multipart );  
	     
	    //7) send message  
	    Transport.send(message);  
	   
	   System.out.println("message sent....");  
	   }catch (MessagingException ex) {ex.printStackTrace();}  
	 
	}
	
	public static List<String> getFileList(File dir) {
	    
		List<String> fileList = new ArrayList<>();
		
	    if(dir.listFiles().length > 0){
	    for (File file : dir.listFiles()) {
	    	System.out.println("List of Files : " + dir.listFiles().length);
	        if (file.isFile()) {
	             System.out.println(file.getName());
	             fileList.add(file.getName());
	            
	        	} 
	    	}
	    }
	    return fileList;
	}
	
}
