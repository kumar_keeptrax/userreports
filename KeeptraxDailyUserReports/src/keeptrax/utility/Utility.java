package keeptrax.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import keeptrax.ConfigPropertyVO;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Utility {
	ConfigPropertyVO configPropertyVO = new ConfigPropertyVO();
	
	/**
	 * @return the configPropertyVO
	 */
	public ConfigPropertyVO getConfigPropertyVO() {
		return configPropertyVO;
	}

	/**
	 * @param configPropertyVO the configPropertyVO to set
	 */
	public void setConfigPropertyVO(ConfigPropertyVO configPropertyVO) {
		this.configPropertyVO = configPropertyVO;
	}

	public String[] getPropValues() throws IOException {
		 
		//Collection<Object> result;
		//String result = "";
        Properties prop = new Properties();
        String propFileName = "userconfig.properties";
 
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        prop.load(inputStream);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
 
         
        // get the property value and print it out
        String user = prop.getProperty("useremail");
        String[] result = user.split(",");
        
        String validEntryTime = prop.getProperty("VALID_ENTRY_TIME");
        configPropertyVO.setValidEntryTime(validEntryTime);
        
        String validExitTime = prop.getProperty("VALID_EXIT_TIME");
        configPropertyVO.setValidExitTime(validExitTime);
        
       
        return result;
    }
	
	public String getFromEmailPropValues() {
		 
		Properties prop = new Properties();
        String propFileName = "emailconfig.properties"; 
        String fromEmailAddress = null;
        try{
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        prop.load(inputStream);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
 
         fromEmailAddress = prop.getProperty("FROM_EMAIL");
        }catch(Exception e){
        	e.printStackTrace();
        }
        
       
        return fromEmailAddress;
    }
	
	public String[] getToEmailPropValues()  {
		 
		Properties prop = new Properties();
        String propFileName = "emailconfig.properties"; 
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        String toEmailAddress = null;
        
        try{
        prop.load(inputStream);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }        
         toEmailAddress = prop.getProperty("TO_EMAIL");
        }catch(Exception e){
        	e.printStackTrace();
        }
        String[] toEmailList = toEmailAddress.split(";");
       
        return toEmailList;
    }
	
	public String getPassword() {
		Properties prop = new Properties();
        String propFileName = "emailconfig.properties"; 
        String password = null;
        
        try{
        InputStream inputStreams = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        prop.load(inputStreams);
        if (inputStreams == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }        
        }catch(Exception e ){
        	e.printStackTrace();
        }
        return password = prop.getProperty("PASSWORD"); 
        
	}
	
	public String getUserReportDir() {
		Properties prop = new Properties();
        String propFileName = "emailconfig.properties"; 
        String userReportsDir = null;
        
        try{
        InputStream inputStreams = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        prop.load(inputStreams);
        if (inputStreams == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }        
        }catch(Exception e ){
        	e.printStackTrace();
        }
        return userReportsDir = prop.getProperty("USER_REPORTS_DIR"); 
        
	}
	
	@SuppressWarnings("unchecked")
	public String[] getDBProperties() {
		Properties prop = new Properties();
        String propFileName = "dbconfig.properties"; 
        String userReportsDir = null;
        
        String[] dbProperties = new String[6];
        
        try{
        InputStream inputStreams = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        prop.load(inputStreams);
        if (inputStreams == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }        
        }catch(Exception e ){
        	e.printStackTrace();
        }
        
       
       
        dbProperties[0] = prop.getProperty("DB_DOMAIN");
        dbProperties[1] = prop.getProperty("DB_PORT");
        dbProperties[2] = prop.getProperty("DB_URI");
        dbProperties[3] = prop.getProperty("DB_DATABASE");
        dbProperties[4] = prop.getProperty("DB_USERNAME");
        dbProperties[5] = prop.getProperty("DB_PASSWORD");
         
        return dbProperties;
        
        
	}
	
	public String convertObjectIDToString(String objForConversion){
		JSONObject json = null;
		String returnString = null;
		try {
			json = (JSONObject)new JSONParser().parse(objForConversion);
			 returnString = json.get("$oid").toString();	
		} catch (ParseException e) {
 			e.printStackTrace();
		}				
		return returnString;
		
	}
	
	
	/*
	 * Entry time validation
	 */
	public boolean isEntryTimeValid(String visitStartHrs){
		
		String validEntryTime = configPropertyVO.getValidEntryTime();
		String validExitTime = configPropertyVO.getValidExitTime();
		
		boolean isStartTimeValid = false;			
		
		//start time validation
		if(visitStartHrs.compareTo(validEntryTime) < 0 || visitStartHrs.compareTo(validEntryTime)==0){
				if(visitStartHrs.compareTo(validExitTime) < 0  ||  visitStartHrs.compareTo(validExitTime)==0){
					System.out.println("Valid visitHrs111");
					isStartTimeValid = true;
				}
				
		} //1st if END

		return isStartTimeValid;
	}
	
	/*
	 * Exit time validation
	 */
	
	public boolean isExitTimeValid(String visitEndHrs){
			
		String validEntryTime = configPropertyVO.getValidEntryTime();
		String validExitTime = configPropertyVO.getValidExitTime();
		
		boolean isEndTimeValid = false;
		
	
		
		if(visitEndHrs.compareTo(validEntryTime) == 1 || visitEndHrs.compareTo(validEntryTime)==0){
			if(visitEndHrs.compareTo(validExitTime) == -1  ||  visitEndHrs.compareTo(validExitTime)==0){
				System.out.println("Valid visitHrs111");
				isEndTimeValid = true;
			}
		}
		return isEndTimeValid;
	}
	 
	public  BigDecimal getDurationDiff(String entryTime , String exitTime) throws NumberFormatException{
		BigDecimal decimal= null;
		try{
			double exit = Double.parseDouble(new String(exitTime).trim());
			double entry =  Double.parseDouble(new String(entryTime).trim());
			
		double diffHrs =  exit - entry;
		//System.out.println("Diff Hrs :  " + diffHrs);	
		
		
		if ( diffHrs > 0) {
			 decimal = new BigDecimal(String.valueOf(diffHrs)).setScale(2, BigDecimal.ROUND_FLOOR);
	    } else {
	    	 decimal =  new BigDecimal(String.valueOf(diffHrs)).setScale(2, BigDecimal.ROUND_CEILING);
	    }
		
		System.out.println("decimal Hrs :  " + decimal);
		}catch(NumberFormatException ne){
			ne.printStackTrace();
		}
		
		return decimal;
	}
}
