package keeptrax.utility;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class DateUtility {
	private static final String MONGO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	/*Date entryDateTime = null;
	Date exitDateTime = null;
	
	Date visitDate = null;*/
	
	String[] entryDateTime = new String[2];
	String[] exitDateTime = new String[2];
	
	String visitDate = null;
	
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/
	
	
	@SuppressWarnings("deprecation")
	public String[] deserializeEntryDate(Object objDate)  throws  Exception{
	  
	  SimpleDateFormat format = new SimpleDateFormat(MONGO_DATE_FORMAT);
	    try {
	    	if(objDate !=null  & objDate!=""){
	    		
	    		JSONObject json = (JSONObject)new JSONParser().parse(objDate.toString());	    		
	    		String tmpDate = json.get("$date").toString(); //ISO date format
	    		
	    		//Calendar  calDateTime = javax.xml.bind.DatatypeConverter.parseDateTime(tmpDate);
	    		
	    		//String dateFormat = format.format(calDateTime);
	    		//String date = dateFormat.substring(0, 22) + ":" + dateFormat.substring(22);
	    		
	    		String date = tmpDate.substring(0, 10);
	    		String hrsAndMin = tmpDate.substring(11, 16);
	    		
	    		entryDateTime[0] = date;
	    		entryDateTime[1] = hrsAndMin;
	    	}else{
	    		throw new Exception("Invalid Entry Date");
	    	}
	    } catch (Exception e) {
	    	e.getMessage();
	    }
	  return entryDateTime;
	}

	@SuppressWarnings("deprecation")
	public String[] deserializeExitDate(Object objDate) throws  Exception{
		  
		  SimpleDateFormat format = new SimpleDateFormat(MONGO_DATE_FORMAT);
		    try {
		    	if(objDate !=null  & objDate!=""){
		    		JSONObject json = (JSONObject)new JSONParser().parse(objDate.toString());		    		
		    		String tmpDate = json.get("$date").toString();
		    		
		    		String date = tmpDate.substring(0, 10);
		    		String hrsAndMin = tmpDate.substring(11, 16);
		    		
		    		exitDateTime[0] = date;
		    		exitDateTime[1] = hrsAndMin;
		    	}else{
		    		throw new Exception("Invalid Exit Date");
		    	}
		    } catch (Exception e) {
		    	e.getMessage();
		    }
		  return exitDateTime;
		}

	public String getDuration(String entryTimeHrs , String exitTimeHrs){
		
		
		return visitDate;
	}
	
	
}
